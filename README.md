# Yahtzee Game (German Version)

This is a simple game application created with Windows Presentation Foundation (WPF) on .NET Framework 4.7.2

[![Version](https://img.shields.io/badge/Version-1.0-success)](https://gitlab.com/s-heim/yahtzee-game/-/releases) 
[![MIT License](https://img.shields.io/badge/License-MIT-orange)](http://opensource.org/licenses/MIT) 
[![dotnet version](https://img.shields.io/badge/.NET_Framework-v4.7.2-blue)](https://docs.microsoft.com/en-us/dotnet/framework/whats-new/#v472)


## Game Play

The game contains description in german language.

| ![Start Page](media/game.jpg) | ![During Game Play](media/game2.jpg) | ![Game Over](media/gameover.jpg) |
| ----------------------------- | ------------------------------------ | -------------------------------- |


## License

[MIT © SH](LICENSE)

