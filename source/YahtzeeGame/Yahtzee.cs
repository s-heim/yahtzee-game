﻿using System;
using System.Linq;

namespace YahtzeeGame
{
    /// <summary>
    /// Manager for a Yahtzee ("Kniffel") game.
    /// </summary>
    class Yahtzee
    {
        /// <summary>
        /// The random generator.
        /// </summary>
        private readonly Random random;

        /// <summary>
        /// The states of the dices.
        /// </summary>
        private readonly bool[] diceStatus;

        /// <summary>
        /// The states of the number controls.
        /// </summary>
        private readonly bool[] numberStatus;

        /// <summary>
        /// The states of the special controls.
        /// </summary>
        private readonly bool[] specialStatus;

        /// <summary>
        /// The current values of the dices.
        /// </summary>
        private readonly int[] diceValues;

        /// <summary>
        /// The current number points
        /// </summary>
        private int numberPoints;

        /// <summary>
        /// The current special points
        /// </summary>
        private int specialPoints;

        /// <summary>
        /// The dicing counter
        /// </summary>
        private int counter;

        /// <summary>
        /// Initializes a new instance of the <see cref="Yahtzee"/> class.
        /// </summary>
        public Yahtzee()
        {
            this.random = new Random();
            this.diceStatus = new bool[5];
            this.numberStatus = new bool[7];
            this.specialStatus = new bool[7];
            this.diceValues = new int[5];
            this.numberPoints = 0;
            this.specialPoints = 0;
            this.StartDicing();
            for (int i = 0; i < 7; i++)
            {
                this.numberStatus[i] = false;
                this.specialStatus[i] = false;
            }
        }

        /// <summary>
        /// Starts the dicing.
        /// </summary>
        public void StartDicing()
        {
            for (int i = 0; i < 5; i++)
            {
                this.diceStatus[i] = false;
                this.diceValues[i] = 0;
                this.counter = 0;
            }
        }

        /// <summary>
        /// Dices this instance.
        /// </summary>
        /// <returns>
        /// The diced values.
        /// </returns>
        public int[] Dice()
        {
            if (counter < 3)
            {
                int dicedNumber;
                for (int i = 0; i < 5; i++)
                {
                    if (!diceStatus[i])
                    {
                        dicedNumber = random.Next(1, 7);
                        this.diceValues[i] = dicedNumber;
                    }
                }
                counter++;
                return this.diceValues;
            }
            return null;
        }

        /// <summary>
        /// Fixes the choosed dice.
        /// </summary>
        /// <param name="index">The index of the dice..</param>
        /// <returns>
        ///     <c>true</c> if [is fixed], <c>false</c> if [is unfixed]
        /// </returns>
        public bool FixDice(int index)
        {
            if (!diceStatus[index])
            {
                diceStatus[index] = true;
                return true;
            }
            else
            {
                diceStatus[index] = false;
                return false;
            }
        }

        /// <summary>
        /// Chooses the numbers control.
        /// </summary>
        /// <param name="index">The index of the control.</param>
        /// <returns>
        /// The gained points.
        /// </returns>
        public int ChooseNumber(int index)
        {
            if (!numberStatus[index])
            {
                int numbers = 0;
                foreach (int value in diceValues)
                {
                    if (value == (index + 1))
                    {
                        numbers += index + 1;
                    }
                }
                numberPoints += numbers;
                numberStatus[index] = true;
                return numbers;
            }
            return 0;
        }

        /// <summary>
        /// Chooses the bonus control.
        /// </summary>
        /// <returns>
        /// The gained points.
        /// </returns>
        public int ChooseBonus()
        {
            if (!numberStatus[6])
            {
                int test = 0;
                for (int i = 0; i < 7; i++)
                {
                    if (numberStatus[i])
                    {
                        test += 1;
                    }
                }
                if (test == 6)
                {
                    numberStatus[6] = true;
                    if (numberPoints >= 63)
                    {
                        numberPoints += 35;
                        return 35;
                    }
                    return 0;
                }
            }
            return -1;
        }

        /// <summary>
        /// Chooses the multiples of a kind control.
        /// </summary>
        /// <param name="index">The index of the control.</param>
        /// <returns>
        /// The gained points.
        /// </returns>
        public int ChooseMultiplesOfAKind(int index)
        {
            if (!specialStatus[index] && ((index == 0) || (index == 1)))
            {
                int count;
                int value = 0;
                int[] testValues = diceValues;
                Array.Sort(testValues);
                for (int i = 0; i < 5; i++)
                {
                    count = Array.LastIndexOf(testValues, testValues[i]) - Array.IndexOf(testValues, testValues[i]) + 1;
                    if ((count >= 3 + index))
                    {
                        value = count * testValues[Array.IndexOf(testValues, testValues[i])];
                    }
                }
                this.specialPoints += value;
                this.specialStatus[index] = true;
                return value;
            }
            return -1;
        }

        /// <summary>
        /// Chooses the full house control.
        /// </summary>
        /// <returns>
        /// The gained points.
        /// </returns>
        public int ChooseFullHouse()
        {
            if (!specialStatus[2])
            {
                int value = 0;
                int[] testValues = this.diceValues;
                Array.Sort(testValues);
                if (((testValues[0] == testValues[1]) && (testValues[2] == testValues[4])) ^ ((testValues[0] == testValues[2]) && (testValues[3] == testValues[4])))
                {
                    value = 25;
                }
                this.specialPoints += value;
                this.specialStatus[2] = true;
                return value;
            }
            return -1;
        }

        /// <summary>
        /// Chooses the straight control.
        /// </summary>
        /// <param name="index">The index of the control.</param>
        /// <returns>
        /// The gained points.
        /// </returns>
        public int ChooseStraight(int index)
        {
            if (!specialStatus[index])
            {
                int value = 0;
                int[] testValues = this.diceValues.Distinct().ToArray();
                Array.Sort(testValues);
                int straight = 1;
                for (int i = 0; i < testValues.Length - 1; i++)
                {
                    if (testValues[i] + 1 == testValues[i + 1])
                    {
                        straight++;
                    }
                    else
                    {
                        straight = 1;
                    }
                }
                if ((straight == 5) && (index == 4))
                {
                    value = 40;
                }
                else if ((straight >= 4) && (index == 3))
                {
                    value = 30;
                }
                this.specialPoints += value;
                this.specialStatus[index] = true;
                return value;
            }
            return -1;
        }

        /// <summary>
        /// Chooses the yathzee control.
        /// </summary>
        /// <returns>
        /// The gained points.
        /// </returns>
        public int ChooseYathzee()
        {
            if (!specialStatus[5])
            {
                int value = 0;
                int[] testValues = this.diceValues;
                Array.Sort(testValues);
                if ((testValues[0] == testValues[4]) && (testValues[0] != 0))
                {
                    value = 50;
                }
                this.specialPoints += value;
                this.specialStatus[5] = true;
                return value;
            }
            return -1;
        }

        /// <summary>
        /// Chooses the chance control.
        /// </summary>
        /// <returns>
        /// The gained points.
        /// </returns>
        public int ChooseChance()
        {
            if (!specialStatus[6])
            {
                int value = 0;
                foreach (int dice in this.diceValues)
                {
                    value += dice;
                }
                this.specialPoints += value;
                this.specialStatus[6] = true;
                return value;
            }
            return -1;
        }

        /// <summary>
        /// Sums up all currently gained points.
        /// </summary>
        /// <returns>
        /// The sum of all points.
        /// </returns>
        public int SumUp()
        {
            return this.numberPoints + this.specialPoints;
        }

        /// <summary>
        /// Determines whether the game is over.
        /// </summary>
        /// <returns>
        ///     <c>true</c> if [game is over]; otherwise <c>false</c>
        /// </returns>
        public bool GameOver()
        {
            if (this.numberStatus[6])
            {
                int test = 0;
                for (int i = 0; i < 7; i++)
                {
                    if (specialStatus[i])
                    {
                        test += 1;
                    }
                    if (test == 7)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

    }
}
