﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace YahtzeeGame
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// <seealso cref="System.Windows.Window" />
    /// <seealso cref="System.Windows.Markup.IComponentConnector" />
    public partial class MainWindow : Window
    {
        /// <summary>
        /// The yahtzee game handler.
        /// </summary>
        private Yahtzee yahtzee;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            yahtzee = new Yahtzee();
        }

        /// <summary>
        /// Resets the dices.
        /// </summary>
        private void ResetDice()
        {
            this.yahtzee.StartDicing();
            Dice1.Content = "*";
            Dice2.Content = "*";
            Dice3.Content = "*";
            Dice4.Content = "*";
            Dice5.Content = "*";
            Dice1.Background = Brushes.LightGray;
            Dice2.Background = Brushes.LightGray;
            Dice3.Background = Brushes.LightGray;
            Dice4.Background = Brushes.LightGray;
            Dice5.Background = Brushes.LightGray;
        }

        /// <summary>
        /// Handles the Click event of the Numbers control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Numbers_Click(object sender, RoutedEventArgs e)
        {
            ((Button) sender).Content = this.yahtzee.ChooseNumber(int.Parse(((Button)sender).Name.Substring("Number".Length, 1)) - 1);
            this.ResetDice();
        }

        /// <summary>
        /// Handles the Click event of the <see cref="Bonus"/> control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Bonus_Click(object sender, RoutedEventArgs e)
        {
            int bonus = this.yahtzee.ChooseBonus();
            if (bonus != -1)
            {
                Bonus.Content = bonus;
                this.ResetDice();
            }
        }

        /// <summary>
        /// Handles the Click event of the MultiplesOfKind control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void MultiplesOfKind_Click(object sender, RoutedEventArgs e)
        {
            int special = this.yahtzee.ChooseMultiplesOfAKind(int.Parse(((Button)sender).Name.Substring("MultiplesOfKind".Length, 1)) - 3);
            {
                ((Button) sender).Content = special;
                this.ResetDice();
            }
        }

        /// <summary>
        /// Handles the Click event of the <see cref="FullHouse"/> control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void FullHouse_Click(object sender, RoutedEventArgs e)
        {
            int special = this.yahtzee.ChooseFullHouse();
            if (special != -1)
            {
                FullHouse.Content = special;
                this.ResetDice();
            }
        }

        /// <summary>
        /// Handles the Click event of the Straight control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Straight_Click(object sender, RoutedEventArgs e)
        {
            int special = this.yahtzee.ChooseStraight(int.Parse(((Button)sender).Name.Substring("Straight".Length, 1)));
            if (special != -1)
            {
                ((Button) sender).Content = special;
                this.ResetDice();
            }
        }

        /// <summary>
        /// Handles the Click event of the <see cref="Yahtzee"/> control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Yahtzee_Click(object sender, RoutedEventArgs e)
        {
            int special = this.yahtzee.ChooseYathzee();
            if (special != -1)
            {
                Yahtzee.Content = special;
                this.ResetDice();
            }
        }

        /// <summary>
        /// Handles the Click event of the <see cref="Chance"/> control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Chance_Click(object sender, RoutedEventArgs e)
        {
            int special = this.yahtzee.ChooseChance();
            if (special != -1)
            {
                Chance.Content = special;
                this.ResetDice();
            }
        }

        /// <summary>
        /// Handles the Click event of the Dices controls.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Dices_Click(object sender, RoutedEventArgs e)
        {
            if(this.yahtzee.FixDice(int.Parse(((Button)sender).Name.Substring("Dice".Length, 1)) - 1))
            {
                ((Button)sender).Background = Brushes.Gray;
            }
            else
            {
                ((Button)sender).Background = Brushes.LightGray;
            }
        }

        /// <summary>
        /// Handles the Click event of the <see cref="Dice"/> control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Dicing_Click(object sender, RoutedEventArgs e)
        {
            int[] dicing = this.yahtzee.Dice();
            if (dicing != null)
            {
                Dice1.Content = dicing[0];
                Dice2.Content = dicing[1];
                Dice3.Content = dicing[2];
                Dice4.Content = dicing[3];
                Dice5.Content = dicing[4];
            }         
        }

        /// <summary>
        /// Handles the Click event of the <see cref="Sum"/> control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Summing_Click(object sender, RoutedEventArgs e)
        {
            Sum.Content = this.yahtzee.SumUp();
            if (this.yahtzee.GameOver())
            {
                Restart.Foreground = Brushes.Red;
            }
        }

        /// <summary>
        /// Handles the Click event of the <see cref="Restart"/> control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Restart_Click(object sender, RoutedEventArgs e)
        {
            this.ResetDice();
            this.yahtzee = new Yahtzee();
            Number1.Content = "(nur 1er)";
            Number2.Content = "(nur 2er)";
            Number3.Content = "(nur 3er)";
            Number4.Content = "(nur 4er)";
            Number5.Content = "(nur 5er)";
            Number6.Content = "(nur 6er)";
            Bonus.Content = "(Bonus)";
            MultiplesOfKind3.Content = "(Dreierpasch)";
            MultiplesOfKind4.Content = "(Viererpasch)";
            FullHouse.Content = "(Full House)";
            Straight3.Content = "(Kleine Straße)";
            Straight4.Content = "(Große Straße)";
            Yahtzee.Content = "(Kniffel)";
            Chance.Content = "(Chance)";           
            Sum.Content = "Summe";
            Restart.Foreground = Brushes.Black;

        }
    }
}
